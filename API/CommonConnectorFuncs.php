<?php
require_once ('/var/www/oditek.in/public_html/connector/include/dbconfig.php'); 
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "off") ? "https" : "http";
$imagepath=$protocol. "://" . $_SERVER['HTTP_HOST']."/connector/upload/";
$GLOBALS['image']=$imagepath;
class CommonConnectorFuncs{
	 //public $dbcon = "";

	
	 function __construct() {
		
    }
    // destructor
    function __destruct() {
        // $this->close();
    }
	
	//public function setDBConnection($connect1) {
	//	$dbcon1 = $connect1;	
    //}
	
	
    public function insertUserRecordForSignup($connect,$name,$email,$password,$mobile,$device_id, $device_type,$login_type,$con_code,$oauth_provider,$oauth_uid,$login_code,$rawpassword,$otp,$date,$otpDate,$type,$usertype){
    	$sql="INSERT INTO cn_user_info(address,image,date,login_type,social_type,name,email) VALUES ('','','".$date."','".$type."','".$usertype."','".$name."','".$email."')";
		$quer_res=mysqli_query($connect,$sql);
		if($quer_res){
			$sql=mysqli_query($connect,"select * from cn_user_info where email='".$email."'");
			$row=mysqli_fetch_array($sql);
			$user_id=$row['user_id'];
		}
		if($user_id !=''){
			$psql="INSERT INTO cn_user_login (user_id,code,mobile,password,raw_password,status,online_status,otp,con_code,otp_date,oauth_provider,oauth_uid) VALUES ('".$user_id."','".$login_code."','".$mobile."','".$rawpassword."','".$password."',0,0,'".$otp."','".$con_code."','".$otpDate."','".$oauth_provider."','".$oauth_uid."')";
            $quer_res1=mysqli_query($connect,$psql);
            if($quer_res1){
            	$dsql="INSERT INTO cn_user_deviceinfo (device_id,device_type,token_id,userid) VALUES ('".$device_id."','".$device_type."','','".$user_id."')";
                $quer_res2=mysqli_query($connect,$dsql);
                if($quer_res2){
                	$sqlchk=mysqli_query($connect,"select * from cn_user_info where userid='".$user_id."'");
					while($row=mysqli_fetch_array($sqlchk)){
			    		$name=$row['name'];
			    		$email=$row['email'];
			    		$login_code=$login_code;
			    	}
                    $sqllogin=mysqli_query($connect,"select * from cn_user_login where user_id='".$user_id."'");
                    while ($row1=mysqli_fetch_array($sqllogin)) {
                        $mobileno=$row1['mobile'];
                        $con_code=$row1['con_code'];
                    }
			    	$data = array("msg" => "success","name"=>$name,"email"=>$email,"login_code"=>$login_code,"userid"=>$user_id,"status" => 1,"otp"=>$otp,"mobile"=>$mobileno,"con_code"=>$con_code);
			    	return $data;
                }
            }
		}
    	
    }
    public function updateUserOTP($connect,$userid,$code){
        $otp=0;
    	$sql = 'UPDATE cn_user_login SET code="'.$code.'",otp="'.$otp.'",status=1 WHERE user_id="'.$userid.'"';
		//echo $sql;exit;
		$upsql=mysqli_query($connect,$sql);
		if($upsql){
			$data = array("msg" => "success","login_code"=>$code,"userid"=>$userid,"status" => 1);
			return $data;
		}
    }
    public function updateUserResendOTP($connect,$userid,$code,$otp,$otpDate){
    	$sql = 'UPDATE cn_user_login SET code="'.$code.'",otp="'.$otp.'",otp_date="'.$otpDate.'" WHERE user_id="'.$userid.'"';
		$upsql=mysqli_query($connect,$sql);
		if($upsql){
			$data = array("msg" => "success","login_code"=>$code,"userid"=>$userid,"status" => 1,"otp"=>$otp);
			return $data;
		}
    }
    public function updateAllSignInData($connect,$mobile,$password,$device_type,$login_code,$userid,$mobileno,$device_id){
    	$sql = 'UPDATE cn_user_login SET code="'.$login_code.'" WHERE mobile="'.$mobile.'"';
		$upsql=mysqli_query($connect,$sql);
		if($upsql){
			$sql1 = 'UPDATE cn_user_deviceinfo SET device_id="'.$device_id.'",device_type="'.$device_type.'" WHERE userid="'.$userid.'"';
			$upsql1=mysqli_query($connect,$sql1);
			if($upsql1){
                $sqllogin=mysqli_query($connect,"select * from cn_user_login where user_id='".$userid."'");
                while ($row1=mysqli_fetch_array($sqllogin)) {
                    $mobileno=$row1['mobile'];
                    $con_code=$row1['con_code'];
                }
                $sqlinfo=mysqli_query($connect,"select * from cn_user_info where user_id='".$userid."'");
                while($row1=mysqli_fetch_array($sqlinfo)){
                    $name=$row1['name'];
                    $email=$row1['email'];
                }
				$data = array("msg" => "success","login_code"=>$login_code,"userid"=>$userid,"con_code"=> $con_code,"mobile"=>$mobileno,"name"=> $name,"email"=>$email,"status" => 1);
				return $data;
			}
		}
    }
    public function updateDataForResetPass($connect,$mobile,$user_id, $login_code){
        $otp=1111;
    	$sql = 'UPDATE cn_user_login SET otp="'.$otp.'" WHERE mobile="'.$mobile.'"';
        $upsql=mysqli_query($connect,$sql);
        if($upsql){
            $data = array("msg" => "success","otp"=>$otp,"user_id"=>$user_id,"login_code"=> $login_code,"status" =>1);
            return $data;
        }
    }
    public function updateDataForChangePass($connect,$userid,$password,$login_code,$pass){
    	$sql = 'UPDATE cn_user_login SET password="'.$pass.'",raw_password="'.$password.'",code="'.$login_code.'" WHERE user_id="'.$userid.'"';
        $upsql=mysqli_query($connect,$sql);
        if($upsql){
            $data = array("msg" => "success","user_id"=>$userid,"login_code"=> $login_code,"status" =>1);
            return $data;
        }
    }
    public function insertDataForSyncMobNo($connect,$userid,$contact,$date,$login_code){
        $lastupdate=date('Y-m-d H:i:s');
        $sql=mysqli_query($connect,"select * from cn_user_connection");
        if(mysqli_num_rows($sql) > 0){
            $sqlchk=mysqli_query($connect,"select * from cn_user_connection where userid='".$userid."'");
            if(mysqli_num_rows($sqlchk) > 0){
                $sql = 'UPDATE cn_user_connection SET date="'.$date.'",mobileno="'.$contact.'" WHERE userid="'.$userid.'"';
                $upsql=mysqli_query($connect,$sql);
                if($upsql){
                    $sql1 = 'UPDATE cn_user_sync SET cron_lock=" ",status=1,lastupdate="'.$lastupdate.'" WHERE user_id="'.$userid.'"';
                     $upsql1=mysqli_query($connect,$sql1);
                     if($upsql1){
                        $sqlget=mysqli_query($connect,"select * from cn_user_connection where userid='".$userid."'");
                        while($row=mysqli_fetch_array($sqlget)){
                            $connMob=$row['connection'];
                        }
                        $data = array("msg" => "success","user_id"=>$userid,"status" =>1,"connection"=>$connMob);
                        return $data;
                     }
                }
            }else{
                $sql="INSERT INTO cn_user_connection(userid,date,mobileno,connection) VALUES ('".$userid."','".$date."','".$contact."','')";
                $quer_res=mysqli_query($connect,$sql);
                if($quer_res){
                    $sql1="INSERT INTO cn_user_sync(user_id,lastsync,cron_lock,status,lastupdate) VALUES ('".$userid."','','',1,'".$lastupdate."')";
                    $quer_res1=mysqli_query($connect,$sql1);
                    if($quer_res1){
                        $sqlget=mysqli_query($connect,"select * from cn_user_connection where userid='".$userid."'");
                        while($row=mysqli_fetch_array($sqlget)){
                            $connMob=$row['connection'];
                        }
                        $data = array("msg" => "success","user_id"=>$userid,"status" =>1,"connection"=>$connMob);
                        return $data;
                    }
                }
            }
        }else{
            $sql="INSERT INTO cn_user_connection(userid,date,mobileno,connection) VALUES ('".$userid."','".$date."','".$contact."','')";
            $quer_res=mysqli_query($connect,$sql);
            if($quer_res){
                $sql1="INSERT INTO cn_user_sync(user_id,lastsync,cron_lock,status,lastupdate) VALUES ('".$userid."','','',1,'".$lastupdate."')";
                $quer_res1=mysqli_query($connect,$sql1);
                if($quer_res1){
                    $sqlget=mysqli_query($connect,"select * from cn_user_connection where userid='".$userid."'");
                    while($row=mysqli_fetch_array($sqlget)){
                        $connMob=$row['connection'];
                    }
                    $data = array("msg" => "success","user_id"=>$userid,"status" =>1,"connection"=>$connMob);
                    return $data;
                }
            }
        }
    }
    public function insertSocialSignInData($connect,$device_id,$device_type,$oauth_provider,$oauth_uid,$login_code,$mobileno,$userid){
    	 $sql = 'UPDATE cn_user_login SET code="'.$login_code.'" WHERE  user_id="'.$userid.'"';
        $upsql=mysqli_query($connect,$sql);
        if($upsql){
            $sql1 = 'UPDATE cn_user_deviceinfo SET device_id="'.$device_id.'",device_type="'.$device_type.'" WHERE userid="'.$userid.'"';
            $upsql1=mysqli_query($connect,$sql1);
            if($upsql1){
                $sqllogin=mysqli_query($connect,"select * from cn_user_login where user_id='".$userid."'");
                while ($row1=mysqli_fetch_array($sqllogin)) {
                    $mobileno=$row1['mobile'];
                    $con_code=$row1['con_code'];
                }
                $sqlinfo=mysqli_query($connect,"select * from cn_user_info where user_id='".$userid."'");
                while($row1=mysqli_fetch_array($sqlinfo)){
                    $name=$row1['name'];
                    $email=$row1['email'];
                }
                $data = array("msg" => "success","login_code"=>$login_code,"userid"=>$userid,"con_code"=> $con_code,"mobile"=>$mobileno,"name"=> $name,"email"=>$email,"status" => 1);
                return $data;
            }
        }
    }
    public function userUpdateLocationInfo($connect,$device_id,$device_type,$userid,$logincode,$latitude,$longitude,$city){
        $sqlup='UPDATE cn_user_deviceinfo SET  device_id="'.$device_id.'",device_type="'.$device_type.'" WHERE userid="'.$userid.'"';
        $updevice=mysqli_query($connect,$sqlup);
        $sql=mysqli_query($connect,"select * from cn_user_location_info");
        if(mysqli_num_rows($sql) > 0){
            $sqlqry=mysqli_query($connect,"select * from cn_user_location_info where user_id='".$userid."'");
            if(mysqli_num_rows($sqlqry) >0){
                $sql = 'UPDATE cn_user_location_info SET latitude="'.$latitude.'",longitude="'.$longitude.'",city="'.$city.'" WHERE  user_id="'.$userid.'"';
                $upsql=mysqli_query($connect,$sql);
                if($upsql){
                    $data = array("msg" => "success","login_code"=>$logincode,"userid"=>$userid,"status" => 1);
                    return $data;
                }
            }else{
                $sql="INSERT INTO cn_user_location_info(latitude,longitude,user_id,city) VALUES ('".$latitude."','".$longitude."','".$userid."','".$city."')";
                $quer_res=mysqli_query($connect,$sql);
                if($quer_res){
                     $data = array("msg" => "success","login_code"=>$logincode,"userid"=>$userid,"status" => 1);
                    return $data;
                }
            }
        }else{
            $sql="INSERT INTO cn_user_location_info(latitude,longitude,user_id,city) VALUES ('".$latitude."','".$longitude."','".$userid."','".$city."')";
            $quer_res=mysqli_query($connect,$sql);
            if($quer_res){
                 $data = array("msg" => "success","login_code"=>$logincode,"userid"=>$userid,"status" => 1);
                return $data;
            }
        }
    }
    public function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 1)
            return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }
    public function getToken($length) {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max - 1)];
        }

        return $token;
    }
    function generateRandom() {
        $result = base_convert((float) rand() / (float) getrandmax() * round(microtime(true) * 1000), 10, 36);
        return $result;
    }
	
	///added by Chinmaya
	
	public function getUserSyncArray($connect)
	{
		 $sql="SELECT * from cn_user_sync where status='1' ";
		 $quer_res= mysqli_query($connect,$sql);
         return $quer_res;	
	}
	
	public function addCronLock($connect,$user_id,$cron_id)
	{
		 $sql="update cn_user_sync set cron_lock='".$cron_id."' where user_id='".$user_id."' ";
		 $quer_res=mysqli_query($connect,$sql);
	}
	
	public function removeCronLock($connect,$user_id,$cron_id)
	{
		 $sql="update cn_user_sync set cron_lock='0' where user_id='".$user_id."' and cron_lock='".$cron_id."' ";
		 $quer_res=mysqli_query($connect,$sql);
	}
	
	public function updateSyncStatus($connect,$user_id)
	{
		$date = date("d-m-Y h:i A");
		$sql="update cn_user_sync set status='0', lastupdate='".$date."' where user_id='".$user_id."' ";
		$quer_res=mysqli_query($connect,$sql);
	}
	
	public function checkCronLock($connect,$user_id)
	{
		 $cronlock=0;
		 $sql="SELECT * from cn_user_sync where user_id='".$user_id."' ";
		 $quer_res=mysqli_query($connect,$sql);
		 if($row = mysqli_fetch_array($quer_res))
		 {
			 $cronlock = $row['cron_lock'];
		 }
         return $cronlock;	
	}
	
	public function getRecords($connect,$records)
	{
		return mysqli_num_rows($records);
	}
	
	public function getUserMobile($connect,$user_id)
	{
		 $mobile="";
		 $sql="SELECT * from cn_user_login where user_id='".$user_id."' ";
		 $quer_res=mysqli_query($connect,$sql);
		 if($row = mysqli_fetch_array($quer_res))
		 {
			 $mobile = $row['mobile'];
		 }
         return $mobile;
	}
	
	public function getUserIdFromMobile($connect,$mobile)
	{
    		$user_id="";
    		//$sql="SELECT * from cn_user_login where mobile='".$mobiletoSearch."' ";
            $sql="SELECT * from cn_user_login where '".$mobile."' like concat('%', mobile) ";
    		$quer_res=mysqli_query($connect,$sql);
    		if($row = mysqli_fetch_array($quer_res))
    		{
    			$user_id = $row['user_id'];
    		}
            return $user_id;
	}
	
	public function getUserContactMobile($connect,$user_id)
	{
		 $mobilelist="";
		 $sql="SELECT * from cn_user_connection where userid='".$user_id."' ";
		 $quer_res=mysqli_query($connect,$sql);
		 if($row = mysqli_fetch_array($quer_res))
		 {
			 $mobilelist = $row['mobileno'];
		 }
         return $mobilelist;
	}
	
	public function checkAndAddMobileInArray($connect,$list_arr,$mobile)
	{
		$ret=0;
		/*if(!in_array($mobile, $list_arr))
		{
			$ret=1;
			$list_arr[] = $mobile;
		}*/
        $flag=0;
        foreach($list_arr as $value){
            if(strpos($value,$mobile) || strpos($mobile,$value) || $mobile==$value){
                $flag=1;
                break;
            }else{
                $flag=0;
            }
        }
        if($flag==0){
            $ret=1;
            $list_arr[] = $mobile;
        }
		return array($ret,$list_arr);
	}
	
	
	public function findFirstConnection($connect,$user_id)
	{
		$list_arr = Array();
		$level1_arr = Array();
		$level2_arr = Array();
		$level3_arr = Array();
		$mobile = $this->getUserMobile($connect,$user_id);
		$mobilelist = $this->getUserContactMobile($connect,$user_id);
		$mobileArray = explode("," , $mobilelist);
		foreach($mobileArray as $mobj)
		{ 
			//echo ":1:".$mobj."<BR>";
			$mobj = str_replace(' ','',$mobj);
			//echo ":2:".$mobj."<BR>";
			//if(($mobile != $mobj) && (!strpos($mobj,$mobile)))
			if(!strpos($mobj,$mobile))
			{
				$search_user_id = $this->getUserIdFromMobile($connect,$mobj);
				if($search_user_id > 0)
				{
                    echo ":1:".$mobj."::".$search_user_id."<BR>";
					list($ret,$list_arr) = $this->checkAndAddMobileInArray($connect,$list_arr,$mobj);
					if($ret)
						$level1_arr[] = $mobj;
					list($list_arr,$level2_arr,$level3_arr) = $this->findSecondConnection($connect,$mobile,$search_user_id,$list_arr,$level2_arr,$level3_arr);
				}
			}
		}
        //print_r($level1_arr);
		$this->updateConnectionLevelData($connect,$mobile,$level1_arr,$level2_arr,$level3_arr);
		
		$count = count($list_arr);
		return $count;
	}
	
	
	public function findSecondConnection($connect,$mobile,$user_id,$list_arr,$level2_arr,$level3_arr)
	{
		$mobilelist = $this->getUserContactMobile($connect,$user_id);
		$mobileArray = explode("," , $mobilelist);
		foreach($mobileArray as $mobj)
		{
			if(($mobile != $mobj) && (!strpos($mobj,$mobile)))
			{
				$search_user_id = $this->getUserIdFromMobile($connect,$mobj);
				if($search_user_id > 0)
				{
					echo ":2:".$mobj."::".$search_user_id."<BR>";
					list($ret,$list_arr) = $this->checkAndAddMobileInArray($connect,$list_arr,$mobj);
					if($ret)
						$level2_arr[] = $mobj;
					list($list_arr,$level3_arr) = $this->findThirdConnection($connect,$mobile,$search_user_id,$list_arr,$level3_arr);
				}
			}
		}
		return array($list_arr,$level2_arr,$level3_arr);
	}
	
	public function findThirdConnection($connect,$mobile,$user_id,$list_arr,$level3_arr)
	{
		$mobilelist = $this->getUserContactMobile($connect,$user_id);
		$mobileArray = explode("," , $mobilelist);
		foreach($mobileArray as $mobj)
		{
			if(($mobile != $mobj) && (!strpos($mobj,$mobile)))
			{
				$search_user_id = $this->getUserIdFromMobile($connect,$mobj);
				if($search_user_id > 0)
				{
					echo ":3:".$mobj."::".$search_user_id."<BR>";
					list($ret,$list_arr)  = $this->checkAndAddMobileInArray($connect,$list_arr,$mobj);
					if($ret)
						$level3_arr[] = $mobj;
				}
			}
		}
		return array($list_arr,$level3_arr);
	}
	
	public function upateConnectionCount($connect,$user_id,$count)
	{
		$sql="update cn_user_connection set connection='".$count."' where userid='".$user_id."' ";
		 $quer_res=mysqli_query($connect,$sql);
	}
	
	
	public function updateConnectionLevelData($connect,$mobile,$level1_arr,$level2_arr,$level3_arr)
	{
		$mobilelist1 = implode(",",$level1_arr);
		$mobilelist2 = implode(",",$level2_arr);
		$mobilelist3 = implode(",",$level3_arr);
		$user_id = $this->getUserIdFromMobile($connect,$mobile);
		$sql="update cn_user_connection set level1='".$mobilelist1."' , level2='".$mobilelist2."' , level3='".$mobilelist3."' where userid='".$user_id."' ";
		$quer_res=mysqli_query($connect,$sql);
	}
    public function getConnectionDetails($connect,$userid,$mobile){
        $detailsArr=array();
        $sqlmob=mysqli_query($connect,"select * from cn_user_login");
        if(mysqli_num_rows($sqlmob) > 0){
            while ($row=mysqli_fetch_array($sqlmob)) {
                $mobArr[]=$row['mobile'];
            }
        }
        $sqlqry=mysqli_query($connect,"select * from cn_user_connection where userid='".$userid."'");
        if(mysqli_num_rows($sqlqry) > 0){
            while($row=mysqli_fetch_array($sqlqry)){
                $label1=$row['level1'];
                $label2=$row['level2'];
                $lable3=$row['level3'];
                if($label1 !=''){
                    $numArr=explode(",",$label1);
                    foreach ($numArr as $v) {
                        foreach ($mobArr as $k) {
                            if(strpos($v,$k) || strpos($k,$v) || $k==$v){
                                $mflag=1;
                                $mobj=$k;
                                break;
                            }else{
                                $mflag=0;
                                $mobj=$v;
                            }
                        }
                        $sqldetals=mysqli_query($connect,"select * from cn_user_login where mobile='".$mobj."'");
                        $row=mysqli_fetch_array($sqldetals);
                        $userID=$row['user_id'];
                        $sqlinfo=mysqli_query($connect,"select * from cn_user_info where user_id='".$userID."'");
                        $row1=mysqli_fetch_array($sqlinfo);
                        $name=$row1['name'];
                        $email=$row1['email'];
                        $type="Doctor";
                        //$type11Arr[]=array("mobile"=>$v,"name"=>$name,"email"=>$email,"type"=>$type);
                        $detailsArr[]=array("label"=>1,"mobile"=>$v,"name"=>$name,"email"=>$email,"type"=>$type,"distance"=>"5.5Km","address"=>"coming soon","rating"=>4,"image"=>"contact_image.png");
                    }
                   // $type12Arr=array("label"=>1,"user_id"=>$userid,"connectionDetails"=>$type11Arr);
                    //array_push($detailsArr,$type12Arr);
                     
                }
                if($label2 != ''){
                    $numArr1=explode(",",$label2);
                    foreach ($numArr1 as $v) {
                        foreach ($mobArr as $k) {
                            if(strpos($v,$k) || strpos($k,$v) || $k==$v){
                                $mflag=1;
                                $mobj=$k;
                                break;
                            }else{
                                $mflag=0;
                                $mobj=$v;
                            }
                        }
                        $sqldetals=mysqli_query($connect,"select * from cn_user_login where mobile='".$mobj."'");
                        $row=mysqli_fetch_array($sqldetals);
                        $userID=$row['user_id'];
                        $sqlinfo=mysqli_query($connect,"select * from cn_user_info where user_id='".$userID."'");
                        $row1=mysqli_fetch_array($sqlinfo);
                        $name=$row1['name'];
                        $email=$row1['email'];
                        $type="Engineer";
                        //$type21Arr[]=array("mobile"=>$v,"name"=>$name,"email"=>$email,"type"=>$type);
                        $detailsArr[]=array("label"=>2,"mobile"=>$v,"name"=>$name,"email"=>$email,"type"=>$type,"distance"=>"7.5Km","address"=>"coming soon","rating"=>5,"image"=>"contact_image.png");
                    }
                    //$type22Arr=array("label"=>2,"user_id"=>$userid,"connectionDetails"=>$type21Arr);
                    //array_push($detailsArr,$type22Arr);
                    

                }
                if($lable3 !=''){
                    $numArr2=explode(",",$lable3);
                    foreach ($numArr2 as $v) {
                        foreach ($mobArr as $k) {
                            if(strpos($v,$k) || strpos($k,$v) || $k==$v){
                                $mflag=1;
                                $mobj=$k;
                                break;
                            }else{
                                $mflag=0;
                                $mobj=$v;
                            }
                        }
                        $sqldetals=mysqli_query($connect,"select * from cn_user_login where mobile='".$mobj."'");
                        $row=mysqli_fetch_array($sqldetals);
                        $userID=$row['user_id'];
                        $sqlinfo=mysqli_query($connect,"select * from cn_user_info where user_id='".$userID."'");
                        $row1=mysqli_fetch_array($sqlinfo);
                        $name=$row1['name'];
                        $email=$row1['email'];
                        $type="Labour";
                       // $type31Arr[]=array("mobile"=>$v,"name"=>$name,"email"=>$email,"type"=>$type);
                        $detailsArr[]=array("label"=>3,"mobile"=>$v,"name"=>$name,"email"=>$email,"type"=>$type,"distance"=>"6.5Km","address"=>"coming soon","rating"=>4.5,"image"=>"contact_image.png");
                    }
                    //$type32Arr=array("label"=>3,"user_id"=>$userid,"connectionDetails"=>$type31Arr);
                    //array_push($detailsArr,$type32Arr);
                    
                }
            }
            $data1=array("data"=>$detailsArr,"imagepath"=>$GLOBALS['image']);
            return $data1;
        }else{
            return $detailsArr;
        }
    }
    public function editContactInfo($connect,$email,$mobileno,$address,$userid,$city,$altmobno,$fileName){
        $sql = 'UPDATE cn_user_info SET email="'.$email.'", address="'.$address.'",city="'.$city.'",alt_mobile_no="'.$altmobno.'",image="'.$fileName.'" WHERE  user_id="'.$userid.'"';
        $upsql=mysqli_query($connect,$sql);
        if($upsql){
            $data = array("msg" => "success","userid"=>$userid,"status" => 1);
            return $data;
        }
    }
    public function setSkillInfo($connect,$type,$cat_id,$subcat_id,$userid,$description){
        $sqlchk=mysqli_query($connect,"select * from cn_user_skills");
        if(mysqli_num_rows($sqlchk) > 0){
            $sqlqry=mysqli_query($connect,"select * from cn_user_skills where user_id='".$userid."'");
            if(mysqli_num_rows($sqlqry) > 0){
                $sql = 'UPDATE cn_user_skills SET skill_type="'.$type.'", cat_id="'.$cat_id.'",subcat_id="'.$subcat_id.'",description="'.$description.'" WHERE  user_id="'.$userid.'"';
                $upsql=mysqli_query($connect,$sql);
                if($upsql){
                   $data=array("status"=>1,"msg"=>"success","userid"=>$userid);
                   return $data; 
                }
            }else{
                $sql="INSERT INTO cn_user_skills(user_id,skill_type,cat_id,subcat_id,description) VALUES ('".$userid."','".$type."','".$cat_id."','".$subcat_id."','".$description."')";
                $quer_res=mysqli_query($connect,$sql);
                if($quer_res){
                    $data=array("status"=>1,"msg"=>"success","userid"=>$userid);
                    return $data;
                }
            }
        }else{
            $sql="INSERT INTO cn_user_skills(user_id,skill_type,cat_id,subcat_id,description) VALUES ('".$userid."','".$type."','".$cat_id."','".$subcat_id."','".$description."')";
            $quer_res=mysqli_query($connect,$sql);
            if($quer_res){
                $data=array("status"=>1,"msg"=>"success","userid"=>$userid);
                return $data;
            }
        }
    }
    public function setUserEducation($education,$userid){
        
    }
}
?>