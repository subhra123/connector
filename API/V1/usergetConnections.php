<?php
require_once ('CommonConnectorFuncs.php');
error_reporting(E_ALL);
ini_set('display_errors', '1');
$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "off") ? "https" : "http";
$imagepath=$protocol. "://" . $_SERVER['HTTP_HOST']."/connector/upload/";
$page=(isset($_REQUEST['pg']))?$_REQUEST['pg']:0;
$sort=(isset($_REQUEST['sort']))?$_REQUEST['sort']:0;
if(isset($_REQUEST['dir'])){
	$dir=($_REQUEST['dir']==2)?'DESC':'ASC';
}else{
	if($sort==0){$dir='DESC';}else{	$dir='ASC';	}
}
$userClass=new CommonConnectorFuncs();
$action=$_REQUEST['action'];
if($action=='getConnections'){
	$userid=$_POST['userid'];
    $login_code=$_POST['login_code'];
    if(!empty($userid) && !empty($login_code)){
    	$sql=mysqli_query($connect,"select * from cn_user_connection where userid='".$userid."'");
    	if(mysqli_num_rows($sql) > 0){
    		while ($row=mysqli_fetch_array($sql)) {
                $mobilenos=$row['connection'];
            }
            $data = array("msg" => "success","user_id"=>$userid,"mobileno"=>$mobilenos,"status" =>1);
    	}else{
    		$data=array("msg"=>"Failed","status"=>0);
    	}
    }else{
    	$data=array("msg"=>"Failed","status"=>0);
    }
    echo json_encode($data);
}
function crypto_rand_secure($min, $max) {
    $range = $max - $min;
    if ($range < 1)
        return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd > $range);
    return $min + $rnd;
}

function getToken($length) {
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet); // edited

    for ($i = 0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, $max - 1)];
    }

    return $token;
}
function generateRandom() {
    $result = base_convert((float) rand() / (float) getrandmax() * round(microtime(true) * 1000), 10, 36);
    return $result;
}
?>