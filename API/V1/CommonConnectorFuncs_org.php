<?php
require_once ('../../include/dbconfig.php'); 
error_reporting(E_ALL);
ini_set('display_errors', '1');
$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "off") ? "https" : "http";
$imagepath=$protocol. "://" . $_SERVER['HTTP_HOST']."/connector/upload/";
class CommonConnectorFuncs{
	 function __construct() {
 
    }
    // destructor
    function __destruct() {
        // $this->close();
    }
    public function insertUserRecordForSignup($connect,$name,$email,$password,$mobile,$device_id, $device_type,$login_type,$con_code,$oauth_provider,$oauth_uid,$login_code,$rawpassword,$otp,$date,$otpDate,$type,$usertype){
    	$sql="INSERT INTO cn_user_info(address,image,date,login_type,social_type,name,email) VALUES ('','','".$date."','".$type."','".$usertype."','".$name."','".$email."')";
		$quer_res=mysqli_query($connect,$sql);
		if($quer_res){
			$sql=mysqli_query($connect,"select * from cn_user_info where email='".$email."'");
			$row=mysqli_fetch_array($sql);
			$user_id=$row['user_id'];
		}
		if($user_id !=''){
			$psql="INSERT INTO cn_user_login (user_id,code,mobile,password,raw_password,status,online_status,otp,con_code,otp_date,oauth_provider,oauth_uid) VALUES ('".$user_id."','".$login_code."','".$mobile."','".$rawpassword."','".$password."',1,0,'".$otp."','".$con_code."','".$otpDate."','".$oauth_provider."','".$oauth_uid."')";
            $quer_res1=mysqli_query($connect,$psql);
            if($quer_res1){
            	$dsql="INSERT INTO cn_user_deviceinfo (device_id,device_type,token_id,userid) VALUES ('".$device_id."','".$device_type."','','".$user_id."')";
                $quer_res2=mysqli_query($connect,$dsql);
                if($quer_res2){
                	$sqlchk=mysqli_query($connect,"select * from cn_user_info where userid='".$user_id."'");
					while($row=mysqli_fetch_array($sqlchk)){
			    		$name=$row['name'];
			    		$email=$row['email'];
			    		$login_code=$login_code;
			    	}
                    $sqllogin=mysqli_query($connect,"select * from cn_user_login where user_id='".$user_id."'");
                    while ($row1=mysqli_fetch_array($sqllogin)) {
                        $mobileno=$row1['mobile'];
                        $con_code=$row1['con_code'];
                    }
			    	$data = array("msg" => "success","name"=>$name,"email"=>$email,"login_code"=>$login_code,"userid"=>$user_id,"status" => 1,"otp"=>$otp,"mobile"=>$mobileno,"con_code"=>$con_code);
			    	return $data;
                }
            }
		}
    	
    }
    public function updateUserOTP($connect,$userid,$code){
        $otp=0;
    	$sql = 'UPDATE cn_user_login SET code="'.$code.'",otp="'.$otp.'" WHERE user_id="'.$userid.'"';
		//echo $sql;exit;
		$upsql=mysqli_query($connect,$sql);
		if($upsql){
			$data = array("msg" => "success","login_code"=>$code,"userid"=>$userid,"status" => 1);
			return $data;
		}
    }
    public function updateUserResendOTP($connect,$userid,$code,$otp,$otpDate){
    	$sql = 'UPDATE cn_user_login SET code="'.$code.'",otp="'.$otp.'",otp_date="'.$otpDate.'" WHERE user_id="'.$userid.'"';
		$upsql=mysqli_query($connect,$sql);
		if($upsql){
			$data = array("msg" => "success","login_code"=>$code,"userid"=>$userid,"status" => 1,"otp"=>$otp);
			return $data;
		}
    }
    public function updateAllSignInData($connect,$mobile,$password,$device_type,$login_code,$userid,$mobileno,$device_id){
    	$sql = 'UPDATE cn_user_login SET code="'.$login_code.'" WHERE mobile="'.$mobile.'"';
		$upsql=mysqli_query($connect,$sql);
		if($upsql){
			$sql1 = 'UPDATE cn_user_deviceinfo SET device_id="'.$device_id.'",device_type="'.$device_type.'" WHERE userid="'.$userid.'"';
			$upsql1=mysqli_query($connect,$sql1);
			if($upsql1){
                $sqllogin=mysqli_query($connect,"select * from cn_user_login where user_id='".$userid."'");
                while ($row1=mysqli_fetch_array($sqllogin)) {
                    $mobileno=$row1['mobile'];
                    $con_code=$row1['con_code'];
                }
                $sqlinfo=mysqli_query($connect,"select * from cn_user_info where user_id='".$userid."'");
                while($row1=mysqli_fetch_array($sqlinfo)){
                    $name=$row1['name'];
                    $email=$row1['email'];
                }
				$data = array("msg" => "success","login_code"=>$login_code,"userid"=>$userid,"con_code"=> $con_code,"mobile"=>$mobileno,"name"=> $name,"email"=>$email,"status" => 1);
				return $data;
			}
		}
    }
    public function updateDataForResetPass($connect,$mobile,$user_id, $login_code){
        $otp=1111;
    	$sql = 'UPDATE cn_user_login SET otp="'.$otp.'" WHERE mobile="'.$mobile.'"';
        $upsql=mysqli_query($connect,$sql);
        if($upsql){
            $data = array("msg" => "success","otp"=>$otp,"user_id"=>$user_id,"login_code"=> $login_code,"status" =>1);
            return $data;
        }
    }
    public function updateDataForChangePass($connect,$userid,$password,$login_code,$pass){
    	$sql = 'UPDATE cn_user_login SET password="'.$pass.'",raw_password="'.$password.'",code="'.$login_code.'" WHERE user_id="'.$userid.'"';
        $upsql=mysqli_query($connect,$sql);
        if($upsql){
            $data = array("msg" => "success","user_id"=>$userid,"login_code"=> $login_code,"status" =>1);
            return $data;
        }
    }
    public function insertDataForSyncMobNo($connect,$userid,$contact,$date,$login_code){
        $lastupdate=date('Y-m-d H:i:s');
        $sql=mysqli_query($connect,"select * from cn_user_connection");
        if(mysqli_fetch_array($sql) > 0){
            $sqlchk=mysqli_query($connect,"select * from cn_user_connection where userid='".$userid."'");
            if(mysqli_fetch_array($sqlchk) > 0){
                $sql = 'UPDATE cn_user_connection SET date="'.$date.'",mobileno="'.$contact.'" WHERE userid="'.$userid.'"';
                $upsql=mysqli_query($connect,$sql);
                if($upsql){
                    $sql1 = 'UPDATE cn_user_sync SET cron_lock="",status=1,lastupdate="'.$lastupdate.'" WHERE user_id="'.$userid.'"';
                     $upsql1=mysqli_query($connect,$sql1);
                     if($upsql1){
                        $sqlget=mysqli_query($connect,"select * from cn_user_connection where userid='".$userid."'");
                        while($row=mysqli_fetch_array($sqlget)){
                            $connMob=$row['connection'];
                        }
                        $data = array("msg" => "success","user_id"=>$userid,"status" =>1,"connection"=>$connMob);
                        return $data;
                     }
                }
            }else{
                $sql="INSERT INTO cn_user_connection(userid,date,mobileno,connection) VALUES ('".$userid."','".$date."','".$contact."','')";
                $quer_res=mysqli_query($connect,$sql);
                if($quer_res){
                    $sql1="INSERT INTO cn_user_sync(user_id,lastsync,cron_lock,status,lastupdate) VALUES ('".$userid."','','',1,'".$lastupdate."')";
                    $quer_res1=mysqli_query($connect,$sql1);
                    if($quer_res1){
                        $sqlget=mysqli_query($connect,"select * from cn_user_connection where userid='".$userid."'");
                        while($row=mysqli_fetch_array($sqlget)){
                            $connMob=$row['connection'];
                        }
                        $data = array("msg" => "success","user_id"=>$userid,"status" =>1,"connection"=>$connMob);
                        return $data;
                    }
                }
            }
        }else{
            $sql="INSERT INTO cn_user_connection(userid,date,mobileno,connection) VALUES ('".$userid."','".$date."','".$contact."','')";
            $quer_res=mysqli_query($connect,$sql);
            if($quer_res){
                $sql1="INSERT INTO cn_user_sync(user_id,lastsync,cron_lock,status,lastupdate) VALUES ('".$userid."','','',1,'".$lastupdate."')";
                $quer_res1=mysqli_query($connect,$sql1);
                if($quer_res1){
                    $sqlget=mysqli_query($connect,"select * from cn_user_connection where userid='".$userid."'");
                    while($row=mysqli_fetch_array($sqlget)){
                        $connMob=$row['connection'];
                    }
                    $data = array("msg" => "success","user_id"=>$userid,"status" =>1,"connection"=>$connMob);
                    return $data;
                }
            }
        }
    }
    public function insertSocialSignInData($connect,$device_id,$device_type,$oauth_provider,$oauth_uid,$login_code,$mobileno,$userid){
    	 $sql = 'UPDATE cn_user_login SET code="'.$login_code.'" WHERE  user_id="'.$userid.'"';
        $upsql=mysqli_query($connect,$sql);
        if($upsql){
            $sql1 = 'UPDATE cn_user_deviceinfo SET device_id="'.$device_id.'",device_type="'.$device_type.'" WHERE userid="'.$userid.'"';
            $upsql1=mysqli_query($connect,$sql1);
            if($upsql1){
                $sqllogin=mysqli_query($connect,"select * from cn_user_login where user_id='".$userid."'");
                while ($row1=mysqli_fetch_array($sqllogin)) {
                    $mobileno=$row1['mobile'];
                    $con_code=$row1['con_code'];
                }
                $sqlinfo=mysqli_query($connect,"select * from cn_user_info where user_id='".$userid."'");
                while($row1=mysqli_fetch_array($sqlinfo)){
                    $name=$row1['name'];
                    $email=$row1['email'];
                }
                $data = array("msg" => "success","login_code"=>$login_code,"userid"=>$userid,"con_code"=> $con_code,"mobile"=>$mobileno,"name"=> $name,"email"=>$email,"status" => 1);
                return $data;
            }
        }
    }
    public function userUpdateLocationInfo($connect,$device_id,$device_type,$userid,$logincode,$latitude,$longitude,$city){
        $sqlup='UPDATE cn_user_deviceinfo SET  device_id="'.$device_id.'",device_type="'.$device_type.'" WHERE userid="'.$userid.'"';
        $updevice=mysqli_query($connect,$sqlup);
        $sql=mysqli_query($connect,"select * from cn_user_location_info");
        if(mysqli_num_rows($sql) > 0){
            $sqlqry=mysqli_query($connect,"select * from cn_user_location_info where user_id='".$userid."'");
            if(mysqli_num_rows($sqlqry) >0){
                $sql = 'UPDATE cn_user_location_info SET latitude="'.$latitude.'",longitude="'.$longitude.'",city="'.$city.'" WHERE  user_id="'.$userid.'"';
                $upsql=mysqli_query($connect,$sql);
                if($upsql){
                    $data = array("msg" => "success","login_code"=>$logincode,"userid"=>$userid,"status" => 1);
                    return $data;
                }
            }else{
                $sql="INSERT INTO cn_user_location_info(latitude,longitude,user_id,city) VALUES ('".$latitude."','".$longitude."','".$userid."','".$city."')";
                $quer_res=mysqli_query($connect,$sql);
                if($quer_res){
                     $data = array("msg" => "success","login_code"=>$logincode,"userid"=>$userid,"status" => 1);
                    return $data;
                }
            }
        }else{
            $sql="INSERT INTO cn_user_location_info(latitude,longitude,user_id,city) VALUES ('".$latitude."','".$longitude."','".$userid."','".$city."')";
            $quer_res=mysqli_query($connect,$sql);
            if($quer_res){
                 $data = array("msg" => "success","login_code"=>$logincode,"userid"=>$userid,"status" => 1);
                return $data;
            }
        }
    }
    public function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 1)
            return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }
    public function getToken($length) {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max - 1)];
        }

        return $token;
    }
    function generateRandom() {
        $result = base_convert((float) rand() / (float) getrandmax() * round(microtime(true) * 1000), 10, 36);
        return $result;
    }
}
?>